#!/usr/bin/env python
# encoding: utf-8

import csv
import collections
import numpy as np
import numpy.lib.recfunctions
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error, make_scorer

import anfis

with open('abalone.csv') as file:
    reader = csv.reader(file)
    samples = list(reader)

samples = np.rec.array(samples, [
    ('sex', 'a1'),
    ('length', float),
    ('diameter', float),
    ('height', float),
    ('whole_weight', float),
    ('shucked_weight', float),
    ('viscera_weight', float),
    ('shell_weight', float),
    ('rings', float),
])

samples = np.lib.recfunctions.merge_arrays([
    np.rec.array((samples.sex == b'I').astype([('is_infant', float)])),
    np.rec.array((samples.sex == b'F').astype([('is_female', float)])),
    np.rec.array((samples.sex == b'M').astype([('is_male', float)])),
    np.lib.recfunctions.drop_fields(samples, 'sex'),
], flatten=True)

all_input = np.column_stack(samples[n] for n in samples.dtype.names
                                       if n != 'rings')
all_output = samples['rings']

anfis = anfis.Anfis(n_rules=25)

print('Model evaluation:')

scores = cross_val_score(anfis, all_input, all_output,
    scoring=make_scorer(mean_absolute_error), cv=4, n_jobs=-1,
    fit_params={'verbose': True})

print(f'Absolute mean error: {scores.mean():.4g}')


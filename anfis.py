# encoding: utf-8

import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

class Anfis(BaseEstimator, RegressorMixin):
    DEFAULT_FIT_RATE = float(np.sqrt(np.finfo(float).eps))

    def __init__(self, n_rules):
        self.n_rules = n_rules

    def fit(self, x, y, rate=None, verbose=False, **kwargs):
        if y.shape[0] != x.shape[0]:
            raise ValueError()

        if rate is None:
            rate = self.DEFAULT_FIT_RATE

        q = self.n_rules
        n, m = x.shape
        self.n_features_ = m

        min_ = np.min(x, 0)
        max_ = np.max(x, 0)
        self.center_ = \
            np.random.uniform(min_[None], max_[None], (q, m))

        width = max_ - min_
        self.radius_ = np.random.uniform(width / (4 * m), width * 1.25, (q, m))

        self.slope_ = np.ones((q, m))

        self.factor_ = np.ones((q, m))

        self.bias_ = np.ones(q)

        train_x, test_x, train_y, test_y = train_test_split(x, y, test_size=0.2)

        if verbose:
            print(f'{"fit rate":>15}{"absolute mean error":>25}')
        previous_error = np.inf
        while True:
            backup = self._backup()
            self.fit_predict_step(train_x, train_y, rate=rate)
            predicted_test_y = self.predict(test_x)
            error = mean_absolute_error(test_y, predicted_test_y)
            if verbose:
                print(f'{rate:>15.4e}'
                    f'{error:>25.{np.finfo(float).precision + 1}f}')
            if error == previous_error:
                break
            if error > previous_error:
                self._restore(backup)
                rate /= 2
                continue
            rate *= 1.5
            previous_error = error

        return self

    def fit_predict_step(self, x, y=None, *, rate=None, **kwargs):
        if y is not None and y.shape[0] != x.shape[0]:
            raise ValueError()

        if rate is None:
            rate = self.DEFAULT_FIT_RATE

        q = self.n_rules
        n = x.shape[0]

        c = self.center_
        r = self.radius_
        s = self.slope_
        f = self.factor_
        b = self.bias_

        def layer0():
            nonlocal c, r, s

            t = (x[:, None] - c) / r
            t2 = t ** 2
            t2s = t2 ** s

            a = 1 / (1 + t2s)

            u, e = layer1(a)

            if e is not None:
                a2 = a ** 2
                k = t2s * a2
                _2k = 2 * k

                da_dc = _2k / t
                da_dr = s * _2k / r
                da_ds = -np.log(t2) * k

                c = c - rate * np.sum(e[:, :, None] * da_dc, 0)
                self.center_ = c

                r = r - rate * np.sum(e[:, :, None] * da_dr, 0)
                np.maximum(r, 0.01, out=r)
                self.radius_ = r

                s = s - rate * np.sum(e[:, :, None] * da_ds, 0)
                np.maximum(s, 0.1, out=s)
                self.slope_ = s

            return u

        def layer1(g):
            a = np.prod(g, 2)

            u, e = layer2(a)

            if e is not None:
                da_dg = a[:, :, None] / g

                e = np.sum(e[:, :, None] * da_dg, 2)

            return u, e

        def layer2(g):
            k = np.sum(g, 1)

            a = g / k[:, None]

            u, e = layer3(a)

            if e is not None:
                da_dg = np.zeros((n, q, q))
                da_dg[:, np.arange(q), np.arange(q)] = k[:, None]
                da_dg -= g[:, None]
                da_dg /= k[:, None, None]

                e = np.sum(e[:, None, None] * da_dg, 1)

            return u, e

        def layer3(g):
            nonlocal f, b

            k = x @ f.T + b

            a = g * k

            u, e = layer4(a)

            if e is not None:
                da_dg = k
                da_df = g[:, :, None] * x[:, None]
                da_db = g

                e = np.sum(e[:, None] * da_dg, 1)

                f = f - rate * np.sum(e[:, None, None] * da_df, 0)
                self.factor_ = f

                b = b - rate * np.sum(e[:, None] * da_db, 0)
                self.bias_ = b

            return u, e

        def layer4(g):
            u = np.sum(g, 1)

            e = None

            if y is not None:
                e = 2 * (u - y)

            return u, e

        return layer0()

    def _backup(self):
        return (
            self.center_,
            self.radius_,
            self.slope_,
            self.factor_,
            self.bias_,
        )

    def _restore(self, backup):
        (
            self.center_,
            self.radius_,
            self.slope_,
            self.factor_,
            self.bias_,
        ) = backup

    def predict(self, x):
        u = self.fit_predict_step(x)
        return u
